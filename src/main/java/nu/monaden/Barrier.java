package nu.monaden;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class CableCar {
    private Lock lock = new ReentrantLock();
    private Condition toBeBottom = lock.newCondition();
    private Condition toBeTop = lock.newCondition();
    private volatile boolean is_bottom = true;
    private volatile int turn = 0;
    private int n = 10;
    private volatile int bordered = 0;
    private Condition allOnBarrier = lock.newCondition();
    private volatile boolean start = true;
    private volatile boolean full = false;
    private Condition waitingForPassengers = lock.newCondition();
    private volatile boolean accepting_passengers = true;
    private Condition toBeAcceptingPassengers = lock.newCondition();
    private Condition toBeEmpty = lock.newCondition();
    private Condition toBeBottomAndEmpty = lock.newCondition();
    private boolean bordering = false;
    private Condition AllOnTheBarrier = lock.newCondition();
    private Condition passengerOut    = lock.newCondition();
    private int pplGettingOut = 0;

    public void border() throws InterruptedException {
        lock.lock();
        System.out.println("At barrier waiting");
        while(!is_bottom || full) toBeBottom.await();
        sync();
        System.out.println("Synceddd, waiting to go to the top");
        full = true;
        waitingForPassengers.signalAll();
        while (is_bottom) toBeTop.await();
        pplGettingOut++;
        if(pplGettingOut == n){
            System.out.println("Last man getting out");
            toBeEmpty.signalAll();
        } else {
            System.out.println("Getting out");
        }

        lock.unlock();
    }



    public void sync() throws InterruptedException {
        int myTurn;
        lock.lock();
        myTurn = turn % 10;
        bordered++ ;

        if (bordered == n)  { turn++; AllOnTheBarrier.signalAll(); bordered = 0; }
        else while (bordered < n && myTurn == turn) AllOnTheBarrier.await() ;
        lock.unlock();
    }

    public void goDown() throws InterruptedException {
        lock.lock();
        if(start) { start = false; }
        else while (pplGettingOut != n) toBeEmpty.await();
        System.out.println("Got Down");
        pplGettingOut = 0;
        is_bottom = true;
        toBeBottom.signalAll();
        lock.unlock();
    }

    public void goUp() throws InterruptedException {
        lock.lock();
        while(!full) waitingForPassengers.await();
        is_bottom = false;
        full      = false;
        System.out.println("I'm up");
        toBeTop.signalAll();
        lock.unlock();
    }

}


class Skier {
    public static void main(String[] args) {
        CableCar c = new CableCar();

        for (int i = 0; i < 20; i++) {
            Ts2 t1 = new Ts2(c, 100, i);
            t1.start();
        }

        Thread goUp = new Thread() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(1000 + (int) (Math.random() * 1000));
                    } catch (InterruptedException e) {
                    }
                    try {
                        c.goUp();
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        };
        Thread goDown = new Thread() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(1000 + (int) (Math.random() * 1000));
                    } catch (InterruptedException e) {
                    }
                    try {
                        c.goDown();
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        };
        goUp.start();
        goDown.start();

    }
}

class Ts2 extends Thread {
    private CableCar barrier;
    private int Speed;
    private int id;

    public Ts2(CableCar br, int sp, int idn) {
        barrier = br;
        Speed = sp;
        id = idn;
    }

    public void run() {
        while(true) {
            try {
                Thread.sleep(Speed + (int) (Math.random() * 1000));
            } catch (InterruptedException e) {
            }

            try {
                barrier.border();
            } catch (InterruptedException e) {
            }

            try {
                Thread.sleep(Speed + (int) (Math.random() * 1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}



