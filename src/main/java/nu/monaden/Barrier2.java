package nu.monaden;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


class MyThread extends Thread {
    private final int id;
    private final BufferMon buffer;
    public MyThread(BufferMon buf, int id) {
        this.id = id;
        buffer = buf;
    }

    public void run(){
        try { buffer.threadCall(id); }
        catch (InterruptedException e) { e.printStackTrace(); }
    }
}

class BufferMon {
    private final Lock lock = new ReentrantLock();
    private final Condition toBeSomehting  = lock.newCondition();
    int elem = 0;

    public BufferMon(){
        (new MyThread(this, 0)).start();
        (new MyThread(this, 1)).start();
        (new MyThread(this, 2)).start();
    }
    public void threadCall(int id) throws InterruptedException {
        lock.lock();
        System.out.println("thread to enter" + id);
        elem++;
        if(elem != 2) { System.out.println("Thread to block" + id); toBeSomehting.await(); }
        System.out.println("Thread : " + id);
        elem = 2;
        toBeSomehting.signal();
        lock.unlock();
    }

    public static void main(String[] args){
        new BufferMon();
    }
}
