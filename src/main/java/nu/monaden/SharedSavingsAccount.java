package nu.monaden;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SharedSavingsAccount {

    private long currentBalance;

    private Lock lock = new ReentrantLock();
    private Condition money = lock.newCondition();

    public SharedSavingsAccount(long initialBalance){
        currentBalance = initialBalance;
    }

    public void deposit(int amount) {
        lock.lock();
        currentBalance += amount;
        money.signalAll();
        lock.unlock();
    }

    public long withdraw(int amount) throws InterruptedException {
        lock.lock();
        try {
            while(amount > currentBalance) money.await();
            currentBalance -= amount;
            return currentBalance;
        } finally {
            lock.unlock();
        }
    }
}
