package nu.monaden;

public class Petersson {

    private volatile int counter = 0;
    private volatile int turn = 0;
    private boolean b0 = false;
    private boolean b1 = false;

    private void people_east(){
        for(int i = 0; i < 1000000; i++){
            b0 = true;
            turn = 1;
            while(b1 && turn == 1);
            counter++;
            b0 = false;
        }
    }

    private void people_west(){
        for(int i = 0; i < 1000000; i++){
            b1 = true;
            turn = 0;
            while(b0 && turn == 0);
            counter++;
            b1 = false;
        }
    }

    public Petersson(){
        Thread t1 = new Thread(){
            @Override
            public void run(){
                people_east();
                done();
            }
        };

        Thread t2 = new Thread(){
            @Override
            public void run(){
                people_west();
                done();
            }
        };

        t1.start();
        t2.start();
    }

    private void done() {
        System.out.println("counter: " + counter);
    }

    public static void main(String[] args){
       new Petersson();
    }
}

