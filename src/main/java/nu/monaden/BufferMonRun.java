package nu.monaden;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class BufferMonOne<E> {
    private final Lock lock = new ReentrantLock();

    private final Condition toBeEmpty = lock.newCondition();
    private final Condition toBeFull  = lock.newCondition();

    private E buf;
    private int elem = 0;

    public BufferMonOne(E init){
        buf = init;
    }

    public void put(E e) throws InterruptedException {
        lock.lock();
        while (elem == 1) toBeEmpty.await();
        buf = e;
        elem++;
        toBeFull.signal();
        lock.unlock();
    }

    public E get() throws InterruptedException {
        E result;
        lock.lock();
        while (elem == 0) toBeFull.await();
        result = buf;
        elem--;
        toBeEmpty.signal();
        lock.unlock();
        return result;
    }
}
class Consumer extends Thread {
    private final int speed;
    private final BufferMonOne<Integer> buffer;
    private final int P;

    public Consumer(BufferMonOne<Integer> b, int p, int sp){
        speed = sp;
        buffer = b;
        P      = p;
    }

    @Override
    public void run() {
        Integer d = 0;
        for (int i = 0; i < P; i++) {
            try {
                Thread.sleep(speed);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("Consumed ");

            try { d = buffer.get(); }
            catch (InterruptedException e) {}

            System.out.println("---> " + d);
        }
    }
}

class Producer extends Thread {
    private int P;
    private int speed = 10;

    private BufferMonOne<Integer> buf;

    public Producer(BufferMonOne<Integer> b, int p, int sp) {
        buf = b;
        P = p;
        speed = sp;
    }

    @Override
    public void run() {
        for (int i = 0; i < P; i++) {
            try { Thread.sleep(speed); }
            catch (InterruptedException e) { }

            System.out.println("Produced " + i);

            try { buf.put(i); }
            catch (InterruptedException e) { }
        }
    }
}
public class BufferMonRun {
    private BufferMonOne<Integer> buf;

    public BufferMonRun(int p, int spp, int c, int spc){
        Producer pr;
        Consumer cs;
        buf = new BufferMonOne<>(42);
        pr = new Producer(buf, p, spp);
        cs = new Consumer(buf, p, spc);
        pr.start();
        cs.start();
    }
    public static void main(String[] args) {
        new BufferMonRun(10, 1, 10, 1000);
    }
}


